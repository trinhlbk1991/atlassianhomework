package com.example.tle99.atlassianhomework.interactor;

import com.example.tle99.atlassianhomework.model.HipChatMessage;
import com.example.tle99.atlassianhomework.util.RegexUtil;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trinh Le on 7/21/2015.
 * Implements the IParseHipChatMessageInteractor to parse retrieve mentions, emoticons and links
 * from a chat message string, then make a HipChatMessage object
 */
public class ParseHipChatMessageInteractor implements IParseHipChatMessageInteractor {

    private static final String REGEX_MENTION = "@([\\w]+)";
    private static final String REGEX_EMOTICON = "\\(([a-zA-Z0-9]{1,15})\\)";
    private static final String REGEX_LINK = "\\b(([\\w-]+:\\/\\/?|www[.])[^\\s()<>]+(?:\\([\\w\\d]+\\)|([^[:punct:]\\s]|\\/*)))";
    private static final String REGEX_PAGE_TITLE = "<title>(.*)<\\/title>";

    @Override
    public HipChatMessage parseHipChatMessageToModel(String message) {
        HipChatMessage model = new HipChatMessage();
        model.originalMessage = message;
        model.mentions = getMentionsFromMessage(message);
        model.emoticons = getEmoticonsFromMessage(message);
        model.links = getLinksFromMessage(message);
        return model;
    }

    private List<String> getMentionsFromMessage(String message) {
        return RegexUtil.getMatchedStrings(REGEX_MENTION, 1, message);
    }

    private List<String> getEmoticonsFromMessage(String message) {
        return RegexUtil.getMatchedStrings(REGEX_EMOTICON, 1, message);
    }

    private List<HipChatMessage.HipChatLink> getLinksFromMessage(String message) {
        List<String> urls = RegexUtil.getMatchedStrings(REGEX_LINK, 1, message);
        List<HipChatMessage.HipChatLink> result = new ArrayList<>();
        for (String url : urls) {
            HipChatMessage.HipChatLink link = new HipChatMessage.HipChatLink();
            link.url = url;
            link.title = getTitleOfUrl(url);
            result.add(link);
        }
        return result;
    }

    /**
     * Return the page's title of the input url.
     * If there's any error when connecting to the input url (page doesn't exists, no connection,...),
     * then return empty string
     *
     * @param url page's url which you want to get the title
     * @return page's title
     */
    private String getTitleOfUrl(String url) {
        InputStream is;
        String title = null;

        try {
            URL urlObj = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            is = conn.getInputStream();
            title = new String(getBytesArrayFromInputStream(is));
            is.close();
            title = getTitleFromResponse(title);
        } catch (IOException ex) {
            if (title == null) {
                title = "";
            }
        }
        return title;
    }

    /**
     * Read bytes from a InputStream
     *
     * @param input InputStream object
     * @return an array of byte which was read from the InputStream
     * @throws IOException IOException exception while reading data from input stream (if any)
     */
    private static byte[] getBytesArrayFromInputStream(InputStream input) throws IOException {
        byte[] buffer = new byte[8192];
        int bytesRead;
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
        return output.toByteArray();
    }

    /**
     * Return the title of a page by its response string
     *
     * @param response the page content in HTML format
     * @return page's title
     */
    private String getTitleFromResponse(String response) {
        return RegexUtil.getFirstMatchedString(REGEX_PAGE_TITLE, 1, response);
    }

}
