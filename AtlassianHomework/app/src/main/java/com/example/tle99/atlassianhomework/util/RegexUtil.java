package com.example.tle99.atlassianhomework.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Trinh Le on 7/22/2015.
 * Util class provides statics method to work with Regex
 */
public class RegexUtil {

    /**
     * Return a List contains all String which matched with input regex without duplication
     *
     * @param regex input regex
     * @param group the group refer to the content which you want to extract, start from 1
     * @param input input string for extracting matched texts
     * @return list of matched string
     */
    public static List<String> getMatchedStrings(String regex, int group, String input) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);
        List<String> result = new ArrayList<>();
        while (matcher.find()) {
            String matchedString = matcher.group(group);
            if (!result.contains(matchedString))
                result.add(matchedString);
        }
        return result;
    }

    /**
     * Return first string which matched the regex
     *
     * @param regex input regex
     * @param group the group refer to the content which you want to extract, start from 1
     * @param input input string for extracting matched text
     * @return first matched string
     */
    public static String getFirstMatchedString(String regex, int group, String input) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);
        if (matcher.find()) {
            return matcher.group(group);
        }
        return "";
    }

}
