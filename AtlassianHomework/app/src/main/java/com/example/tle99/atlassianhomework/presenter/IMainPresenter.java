package com.example.tle99.atlassianhomework.presenter;

/**
 * Created by Trinh Le on 7/21/2015.
 * Defines all methods for an IMainPresenter implementation
 */
public interface IMainPresenter {

    /**
     * Pass the input message to presenter where is will be processed and parsed to proper Json string
     *
     * @param msg the input message
     */
    void parseMessageToJson(String msg);

}
