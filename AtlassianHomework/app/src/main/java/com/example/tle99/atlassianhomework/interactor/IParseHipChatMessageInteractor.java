package com.example.tle99.atlassianhomework.interactor;

import com.example.tle99.atlassianhomework.model.HipChatMessage;

/**
 * Created by Trinh Le on 7/21/2015.
 * Defines all needed methods for IParseHipChatMessageInteractor implementation, which handles the
 * parsing HipChat message to model
 */
public interface IParseHipChatMessageInteractor {

    /**
     * Parse the plain HipChat message to HipChatMessage model
     *
     * @param message  the plain HipChat message
     */
    HipChatMessage parseHipChatMessageToModel(String message);
}
