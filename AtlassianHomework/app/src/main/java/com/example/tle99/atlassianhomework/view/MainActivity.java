package com.example.tle99.atlassianhomework.view;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tle99.atlassianhomework.R;
import com.example.tle99.atlassianhomework.presenter.IMainPresenter;
import com.example.tle99.atlassianhomework.presenter.MainPresenter;


public class MainActivity extends AppCompatActivity implements IMainView, View.OnClickListener {

    private EditText mEtMessage;
    private TextView mTvResult;
    private ProgressDialog mProgressDialog;

    private IMainPresenter mMainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mEtMessage = (EditText) findViewById(R.id.et_message);
        mTvResult = (TextView) findViewById(R.id.tv_result);
        findViewById(R.id.btn_run).setOnClickListener(this);

        mMainPresenter = new MainPresenter(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mProgressDialog.dismiss();
        mProgressDialog = null;
    }

    @Override
    public void showJsonResult(String jsonResult) {
        mTvResult.setText(jsonResult);
    }

    @Override
    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(this, null, getString(R.string.msg_processing));
        }
        mProgressDialog.show();
    }

    @Override
    public void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    @Override
    public void showErrorMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_run:
                mMainPresenter.parseMessageToJson(mEtMessage.getText().toString());
                break;
        }
    }
}
