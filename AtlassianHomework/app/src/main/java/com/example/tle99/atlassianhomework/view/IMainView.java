package com.example.tle99.atlassianhomework.view;

/**
 * Created by Trinh Le on 7/21/2015.
 * Defines all needed methods for an IMainView implementation.
 */
public interface IMainView {

    /**
     * Display the json result on the screen
     *
     * @param jsonResult the Json which you want to display
     */
    void showJsonResult(String jsonResult);

    /**
     * Show progress dialog to indicate that the application is working
     */
    void showProgressDialog();

    /**
     * Hide the progress dialog
     */
    void hideProgressDialog();

    /**
     * Display error message to inform user when anything wrong happens
     *
     * @param msg error message details
     */
    void showErrorMessage(String msg);
}
