package com.example.tle99.atlassianhomework.presenter;

import android.os.AsyncTask;

import com.example.tle99.atlassianhomework.interactor.IParseHipChatMessageInteractor;
import com.example.tle99.atlassianhomework.interactor.ParseHipChatMessageInteractor;
import com.example.tle99.atlassianhomework.view.IMainView;

import org.json.JSONException;

/**
 * Created by Trinh Le on 7/21/2015.
 * Implements IMainPresenter to parse the HipChatMessage to JSON string and work with IMainView
 * to make the UI work properly
 */
public class MainPresenter implements IMainPresenter {

    private final IMainView mMainView;
    private final IParseHipChatMessageInteractor mParseHipChatMessageInteractor;

    public MainPresenter(IMainView view) {
        this.mMainView = view;
        this.mParseHipChatMessageInteractor = new ParseHipChatMessageInteractor();
    }

    @Override
    public void parseMessageToJson(String msg) {
        new ParseMessageToJsonTask().execute(msg);

    }

    /**
     * AsyncTask handle parsing HipChat message in background and update view after finished
     */
    class ParseMessageToJsonTask extends AsyncTask<String, Void, String> {

        private JSONException mException;

        @Override
        protected void onPreExecute() {
            mMainView.showProgressDialog();
        }

        @Override
        protected void onPostExecute(String s) {
            mMainView.hideProgressDialog();
            if (s != null) {
                mMainView.showJsonResult(s);
            } else if (mException != null) {
                mMainView.showErrorMessage(mException.getMessage());
            }
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                return mParseHipChatMessageInteractor.parseHipChatMessageToModel(params[0]).toJson();
            } catch (JSONException ex) {
                mException = ex;
                return null;
            }
        }
    }


}
