package com.example.tle99.atlassianhomework.model;

import org.json.JSONException;

/**
 * Created by Trinh Le on 7/22/2015.
 * Defines common methods of Model
 */
public interface IModel {
    /**
     * Convert this HipChatMessage object to a string with Json format
     *
     * @return the parsed string in Json format
     */
    String toJson() throws JSONException;
}
