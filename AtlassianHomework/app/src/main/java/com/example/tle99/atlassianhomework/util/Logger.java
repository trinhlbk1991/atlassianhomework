package com.example.tle99.atlassianhomework.util;

import android.util.Log;

import com.example.tle99.atlassianhomework.BuildConfig;


/**
 * Created by Trinh Le on 7/8/2015.
 * Customized Logger which only show logs in DEBUG mode
 */
@SuppressWarnings("WeakerAccess")
public class Logger {
    /**
     * Check if the application is running in DEBUG mode or not
     */
    @SuppressWarnings("SameReturnValue")
    private static boolean isDebug() {
        return BuildConfig.DEBUG;
    }

    public static void e(String tag, String message) {
        if (isDebug()) {
            Log.e(tag, message);
        }
    }

    public static void i(String tag, String message) {
        if (isDebug()) {
            Log.i(tag, message);
        }
    }

    public static void d(String tag, String message) {
        if (isDebug()) {
            Log.d(tag, message);
        }
    }

    public static void w(String tag, String message) {
        if (isDebug()) {
            Log.w(tag, message);
        }
    }

    public static void wtf(String tag, String message) {
        if (isDebug()) {
            Log.wtf(tag, message);
        }
    }
}
