package com.example.tle99.atlassianhomework.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Trinh Le on 7/21/2015.
 * Contains all essential properties of a HipChat message like mentioned people, emoticons, urls,...
 * along with the original message
 */
public class HipChatMessage implements IModel {

    private static final String MENTIONS = "mentions";
    private static final String EMOTICONS = "emoticons";
    private static final String LINKS = "links";

    public String originalMessage;
    public List<String> mentions;
    public List<String> emoticons;
    public List<HipChatLink> links;

    @Override
    public String toJson() throws JSONException {
        JSONObject rootJson = new JSONObject();
        if (mentions.size() > 0) {
            rootJson.put(MENTIONS, new JSONArray(mentions));
        }
        if (emoticons.size() > 0) {
            rootJson.put(EMOTICONS, new JSONArray(emoticons));
        }
        if (links.size() > 0) {
            JSONArray arrayLinks = new JSONArray();
            for (HipChatLink link : links) {
                arrayLinks.put(link.toJsonObject());
            }
            rootJson.put(LINKS, arrayLinks);
        }
        return rootJson.toString(4).replaceAll("\\\\", "");
    }

    /**
     * Contains url and title of a link which was mentioned in HipChat message
     */
    public static class HipChatLink implements IModel {
        private static final String TITLE = "title";
        private static final String URL = "url";

        public String url;
        public String title;

        public JSONObject toJsonObject() throws JSONException {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(URL, url);
            jsonObject.put(TITLE, title);
            return jsonObject;
        }

        @Override
        public String toJson() throws JSONException {
            return toJsonObject().toString();
        }
    }
}
