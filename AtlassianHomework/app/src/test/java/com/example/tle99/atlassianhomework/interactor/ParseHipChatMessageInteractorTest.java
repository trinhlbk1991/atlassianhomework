package com.example.tle99.atlassianhomework.interactor;

import android.test.suitebuilder.annotation.SmallTest;

import com.example.tle99.atlassianhomework.model.HipChatMessage;

import junit.framework.TestCase;

import org.junit.Before;

/**
 * Created by Trinh Le on 7/22/2015.
 * Unit test cases for the IParseHipChatMessageInteractor
 */
@SuppressWarnings("unused")
@SmallTest
public class ParseHipChatMessageInteractorTest extends TestCase {

    private static final String LINK_HTTP_URL = "http://www.nbcolympics.com";
    private static final String LINK_HTTP_TITLE = "NBC Olympics | Home of the 2016 Olympic Games in Rio";
    private static final String LINK_HTTPS_URL = "https://google.com";
    private static final String LINK_HTTPS_TITLE = "Google";
    private static final String LINK_PORT_URL = "http://localhost:8080";
    private static final String LINK_PORT_TITLE = "";
    private static final String LINK_QUERY_STRING_URL = "http://regexr.com/foo.html?q=bar";
    private static final String LINK_QUERY_STRING_TITLE = "RegExr: Learn, Build, & Test RegEx";
    private static final String LINK_NON_ASCII_URL = "http://tuyểndụng➡.com";
    private static final String LINK_NON_ASCII_TITLE = "";
    private static final String LINK_IP_URL = "http://223.255.255.254";
    private static final String LINK_IP_TITLE = "";

    private static final String PLAIN_TEXT_MESSAGE = "Hello world!";
    private static final String MESSAGE_WITH_ONE_MENTION_1 = "@Kai Please implement this";
    private static final String MESSAGE_WITH_ONE_MENTION_2 = "@Kai:Please implement this";
    private static final String MESSAGE_WITH_ONE_MENTION_3 = "@_Kai Please implement this";
    private static final String MESSAGE_WITH_ONE_MENTION_4 = "@Kai_ Please implement this";
    private static final String MESSAGE_WITH_ONE_MENTION_5 = "@Kai_123 Please implement this";
    private static final String MESSAGE_WITH_MANY_MENTIONS = "@Kai @harry Please @Harry! implement this @_Kai,@Kai_,@Tom_123@Kai";
    private static final String MESSAGE_WITH_ONE_EMOTICON_1 = "(smile) Good morning!";
    private static final String MESSAGE_WITH_ONE_EMOTICON_2 = "(smile123) Good morning!";
    private static final String MESSAGE_WITH_ONE_EMOTICON_3 = "(smile 123) Good morning!";
    private static final String MESSAGE_WITH_ONE_EMOTICON_4 = "(smile!!!!) Good morning!";
    private static final String MESSAGE_WITH_ONE_EMOTICON_5 = "(smilealittlebitmore) Good morning!";
    private static final String MESSAGE_WITH_ONE_EMOTICON_6 = "(smile Good morning!";
    private static final String MESSAGE_WITH_ONE_EMOTICON_7 = "smile) Good morning!";
    private static final String MESSAGE_WITH_MANY_EMOTICONS = "(smile) (Smile) (sob) Good (smile123)(smile 123) morning!(smile!!!!)(smilealittlebitmore) smile) (smile (sob)";
    private static final String MESSAGE_WITH_ONE_LINK_1 = "Olympics are starting soon; " + LINK_HTTP_URL;
    private static final String MESSAGE_WITH_ONE_LINK_2 = "Olympics are starting soon; " + LINK_PORT_URL;
    private static final String MESSAGE_WITH_ONE_LINK_3 = "Olympics are starting soon; " + LINK_NON_ASCII_URL;
    private static final String MESSAGE_WITH_ONE_LINK_4 = "Olympics are starting soon; " + LINK_HTTPS_URL;
    private static final String MESSAGE_WITH_ONE_LINK_5 = "Olympics are starting soon; " + LINK_QUERY_STRING_URL;
    private static final String MESSAGE_WITH_ONE_LINK_6 = "Olympics are starting soon; " + LINK_IP_URL;
    private static final String MESSAGE_WITH_MANY_LINKS_1 = "Olympics are " + LINK_QUERY_STRING_URL + " starting soon; " + LINK_HTTP_URL;
    private static final String MESSAGE_WITH_MANY_LINKS_2 = LINK_HTTPS_URL + " Olympics are starting soon; " + LINK_HTTP_URL + " " + LINK_HTTP_URL;
    private static final String MESSAGE_MIXED_1 = "@Kai @Kai @John (smile) (smile) (Smile) such a cool feature; " + LINK_HTTPS_URL + " " + LINK_HTTPS_URL;
    private static final String MESSAGE_MIXED_2 = "@Kai @John! such a cool feature; (smile) " + LINK_HTTP_URL + " (smile123) (smile!!!) (smilealittlebitmore)";


    private IParseHipChatMessageInteractor mParser;

    @Before
    protected void setUp() {
        mParser = new ParseHipChatMessageInteractor();
    }

    public void testPlainTextMessage() {
        HipChatMessage model = mParser.parseHipChatMessageToModel(PLAIN_TEXT_MESSAGE);
        assertNotNull(model);
        assertEquals("Incorrect original message", model.originalMessage, PLAIN_TEXT_MESSAGE);
        assertEquals(0, model.emoticons.size());
        assertEquals(0, model.mentions.size());
        assertEquals(0, model.links.size());
    }

    public void testMessageWithOneMention1() {
        HipChatMessage model = mParser.parseHipChatMessageToModel(MESSAGE_WITH_ONE_MENTION_1);
        assertNotNull(model);
        assertEquals("Incorrect original message", model.originalMessage, MESSAGE_WITH_ONE_MENTION_1);
        assertEquals(1, model.mentions.size());
        assertEquals("Kai", model.mentions.get(0));
        assertEquals(0, model.emoticons.size());
        assertEquals(0, model.links.size());
    }

    public void testMessageWithOneMention2() {
        HipChatMessage model = mParser.parseHipChatMessageToModel(MESSAGE_WITH_ONE_MENTION_2);
        assertNotNull(model);
        assertEquals("Incorrect original message", model.originalMessage, MESSAGE_WITH_ONE_MENTION_2);
        assertEquals(1, model.mentions.size());
        assertEquals("Kai", model.mentions.get(0));
        assertEquals(0, model.emoticons.size());
        assertEquals(0, model.links.size());
    }

    public void testMessageWithOneMention3() {
        HipChatMessage model = mParser.parseHipChatMessageToModel(MESSAGE_WITH_ONE_MENTION_3);
        assertNotNull(model);
        assertEquals("Incorrect original message", model.originalMessage, MESSAGE_WITH_ONE_MENTION_3);
        assertEquals(1, model.mentions.size());
        assertEquals("_Kai", model.mentions.get(0));
        assertEquals(0, model.emoticons.size());
        assertEquals(0, model.links.size());
    }

    public void testMessageWithOneMention4() {
        HipChatMessage model = mParser.parseHipChatMessageToModel(MESSAGE_WITH_ONE_MENTION_4);
        assertNotNull(model);
        assertEquals("Incorrect original message", model.originalMessage, MESSAGE_WITH_ONE_MENTION_4);
        assertEquals(1, model.mentions.size());
        assertEquals("Kai_", model.mentions.get(0));
        assertEquals(0, model.emoticons.size());
        assertEquals(0, model.links.size());
    }

    public void testMessageWithOneMention5() {
        HipChatMessage model = mParser.parseHipChatMessageToModel(MESSAGE_WITH_ONE_MENTION_5);
        assertNotNull(model);
        assertEquals("Incorrect original message", model.originalMessage, MESSAGE_WITH_ONE_MENTION_5);
        assertEquals(1, model.mentions.size());
        assertEquals("Kai_123", model.mentions.get(0));
        assertEquals(0, model.emoticons.size());
        assertEquals(0, model.links.size());
    }


    public void testMessageWithManyMentions() {
        HipChatMessage model = mParser.parseHipChatMessageToModel(MESSAGE_WITH_MANY_MENTIONS);
        assertNotNull(model);
        assertEquals("Incorrect original message", model.originalMessage, MESSAGE_WITH_MANY_MENTIONS);
        assertEquals(6, model.mentions.size());
        assertEquals("Kai", model.mentions.get(0));
        assertEquals("harry", model.mentions.get(1));
        assertEquals("Harry", model.mentions.get(2));
        assertEquals("_Kai", model.mentions.get(3));
        assertEquals("Kai_", model.mentions.get(4));
        assertEquals("Tom_123", model.mentions.get(5));
        assertEquals(0, model.emoticons.size());
        assertEquals(0, model.links.size());
    }

    public void testMessageWithOneEmoticon1() {
        HipChatMessage model = mParser.parseHipChatMessageToModel(MESSAGE_WITH_ONE_EMOTICON_1);
        assertNotNull(model);
        assertEquals("Incorrect original message", model.originalMessage, MESSAGE_WITH_ONE_EMOTICON_1);
        assertEquals(1, model.emoticons.size());
        assertEquals("smile", model.emoticons.get(0));
        assertEquals(0, model.mentions.size());
        assertEquals(0, model.links.size());
    }

    public void testMessageWithOneEmoticon2() {
        HipChatMessage model = mParser.parseHipChatMessageToModel(MESSAGE_WITH_ONE_EMOTICON_2);
        assertNotNull(model);
        assertEquals("Incorrect original message", model.originalMessage, MESSAGE_WITH_ONE_EMOTICON_2);
        assertEquals(1, model.emoticons.size());
        assertEquals("smile123", model.emoticons.get(0));
        assertEquals(0, model.mentions.size());
        assertEquals(0, model.links.size());
    }

    public void testMessageWithOneEmoticon3() {
        HipChatMessage model = mParser.parseHipChatMessageToModel(MESSAGE_WITH_ONE_EMOTICON_3);
        assertNotNull(model);
        assertEquals("Incorrect original message", model.originalMessage, MESSAGE_WITH_ONE_EMOTICON_3);
        assertEquals(0, model.emoticons.size());
        assertEquals(0, model.mentions.size());
        assertEquals(0, model.links.size());
    }

    public void testMessageWithOneEmoticon4() {
        HipChatMessage model = mParser.parseHipChatMessageToModel(MESSAGE_WITH_ONE_EMOTICON_4);
        assertNotNull(model);
        assertEquals("Incorrect original message", model.originalMessage, MESSAGE_WITH_ONE_EMOTICON_4);
        assertEquals(0, model.emoticons.size());
        assertEquals(0, model.mentions.size());
        assertEquals(0, model.links.size());
    }

    public void testMessageWithOneEmoticon5() {
        HipChatMessage model = mParser.parseHipChatMessageToModel(MESSAGE_WITH_ONE_EMOTICON_5);
        assertNotNull(model);
        assertEquals("Incorrect original message", model.originalMessage, MESSAGE_WITH_ONE_EMOTICON_5);
        assertEquals(0, model.emoticons.size());
        assertEquals(0, model.mentions.size());
        assertEquals(0, model.links.size());
    }

    public void testMessageWithOneEmoticon6() {
        HipChatMessage model = mParser.parseHipChatMessageToModel(MESSAGE_WITH_ONE_EMOTICON_6);
        assertNotNull(model);
        assertEquals("Incorrect original message", model.originalMessage, MESSAGE_WITH_ONE_EMOTICON_6);
        assertEquals(0, model.emoticons.size());
        assertEquals(0, model.mentions.size());
        assertEquals(0, model.links.size());
    }

    public void testMessageWithOneEmoticon7() {
        HipChatMessage model = mParser.parseHipChatMessageToModel(MESSAGE_WITH_ONE_EMOTICON_7);
        assertNotNull(model);
        assertEquals("Incorrect original message", model.originalMessage, MESSAGE_WITH_ONE_EMOTICON_7);
        assertEquals(0, model.emoticons.size());
        assertEquals(0, model.mentions.size());
        assertEquals(0, model.links.size());
    }

    public void testMessageWithManyEmoticons() {
        HipChatMessage model = mParser.parseHipChatMessageToModel(MESSAGE_WITH_MANY_EMOTICONS);
        assertNotNull(model);
        assertEquals("Incorrect original message", model.originalMessage, MESSAGE_WITH_MANY_EMOTICONS);
        assertNotNull(model.emoticons);
        assertEquals(4, model.emoticons.size());
        assertEquals("smile", model.emoticons.get(0));
        assertEquals("Smile", model.emoticons.get(1));
        assertEquals("sob", model.emoticons.get(2));
        assertEquals("smile123", model.emoticons.get(3));
        assertEquals(0, model.mentions.size());
        assertEquals(0, model.links.size());
    }

    public void testMessageWithOneLink1() {
        HipChatMessage model = mParser.parseHipChatMessageToModel(MESSAGE_WITH_ONE_LINK_1);
        assertNotNull(model);
        assertEquals("Incorrect original message", model.originalMessage, MESSAGE_WITH_ONE_LINK_1);
        assertEquals(1, model.links.size());
        HipChatMessage.HipChatLink link = model.links.get(0);
        assertEquals(LINK_HTTP_URL, link.url);
        assertEquals(LINK_HTTP_TITLE, link.title);
        assertEquals(0, model.emoticons.size());
        assertEquals(0, model.mentions.size());
    }

    public void testMessageWithOneLink2() {
        HipChatMessage model = mParser.parseHipChatMessageToModel(MESSAGE_WITH_ONE_LINK_2);
        assertNotNull(model);
        assertEquals("Incorrect original message", model.originalMessage, MESSAGE_WITH_ONE_LINK_2);
        assertEquals(1, model.links.size());
        HipChatMessage.HipChatLink link = model.links.get(0);
        assertEquals(LINK_PORT_URL, link.url);
        assertEquals(LINK_PORT_TITLE, link.title);
        assertEquals(0, model.emoticons.size());
        assertEquals(0, model.mentions.size());
    }

    public void testMessageWithOneLink3() {
        HipChatMessage model = mParser.parseHipChatMessageToModel(MESSAGE_WITH_ONE_LINK_3);
        assertNotNull(model);
        assertEquals("Incorrect original message", model.originalMessage, MESSAGE_WITH_ONE_LINK_3);
        assertEquals(1, model.links.size());
        HipChatMessage.HipChatLink link = model.links.get(0);
        assertEquals(LINK_NON_ASCII_URL, link.url);
        assertEquals(LINK_NON_ASCII_TITLE, link.title);
        assertEquals(0, model.emoticons.size());
        assertEquals(0, model.mentions.size());
    }

    public void testMessageWithOneLink4() {
        HipChatMessage model = mParser.parseHipChatMessageToModel(MESSAGE_WITH_ONE_LINK_4);
        assertNotNull(model);
        assertEquals("Incorrect original message", model.originalMessage, MESSAGE_WITH_ONE_LINK_4);
        assertEquals(1, model.links.size());
        HipChatMessage.HipChatLink link = model.links.get(0);
        assertEquals(LINK_HTTPS_URL, link.url);
        assertEquals(LINK_HTTPS_TITLE, link.title);
        assertEquals(0, model.emoticons.size());
        assertEquals(0, model.mentions.size());
    }

    public void testMessageWithOneLink5() {
        HipChatMessage model = mParser.parseHipChatMessageToModel(MESSAGE_WITH_ONE_LINK_5);
        assertNotNull(model);
        assertEquals("Incorrect original message", model.originalMessage, MESSAGE_WITH_ONE_LINK_5);
        assertEquals(1, model.links.size());
        HipChatMessage.HipChatLink link = model.links.get(0);
        assertEquals(LINK_QUERY_STRING_URL, link.url);
        assertEquals(LINK_QUERY_STRING_TITLE, link.title);
        assertEquals(0, model.emoticons.size());
        assertEquals(0, model.mentions.size());
    }

    public void testMessageWithOneLink6() {
        HipChatMessage model = mParser.parseHipChatMessageToModel(MESSAGE_WITH_ONE_LINK_6);
        assertNotNull(model);
        assertEquals("Incorrect original message", model.originalMessage, MESSAGE_WITH_ONE_LINK_6);
        assertEquals(1, model.links.size());
        HipChatMessage.HipChatLink link = model.links.get(0);
        assertEquals(LINK_IP_URL, link.url);
        assertEquals(LINK_IP_TITLE, link.title);
        assertEquals(0, model.emoticons.size());
        assertEquals(0, model.mentions.size());
    }

    public void testMessageWithManyLinks1() {
        HipChatMessage model = mParser.parseHipChatMessageToModel(MESSAGE_WITH_MANY_LINKS_1);
        assertNotNull(model);
        assertEquals("Incorrect original message", model.originalMessage, MESSAGE_WITH_MANY_LINKS_1);
        assertEquals(2, model.links.size());
        HipChatMessage.HipChatLink link = model.links.get(0);
        assertEquals(LINK_QUERY_STRING_URL, link.url);
        assertEquals(LINK_QUERY_STRING_TITLE, link.title);
        link = model.links.get(1);
        assertEquals(LINK_HTTP_URL, link.url);
        assertEquals(LINK_HTTP_TITLE, link.title);
        assertEquals(0, model.emoticons.size());
        assertEquals(0, model.mentions.size());
    }

    public void testMessageWithManyLinks2() {
        HipChatMessage model = mParser.parseHipChatMessageToModel(MESSAGE_WITH_MANY_LINKS_2);
        assertNotNull(model);
        assertEquals("Incorrect original message", model.originalMessage, MESSAGE_WITH_MANY_LINKS_2);
        assertEquals(2, model.links.size());
        HipChatMessage.HipChatLink link = model.links.get(0);
        assertEquals(LINK_HTTPS_URL, link.url);
        assertEquals(LINK_HTTPS_TITLE, link.title);
        link = model.links.get(1);
        assertEquals(LINK_HTTP_URL, link.url);
        assertEquals(LINK_HTTP_TITLE, link.title);
        assertEquals(0, model.emoticons.size());
        assertEquals(0, model.mentions.size());
    }

    public void testMessageMixed1() {
        HipChatMessage model = mParser.parseHipChatMessageToModel(MESSAGE_MIXED_1);
        assertNotNull(model);
        assertEquals("Incorrect original message", model.originalMessage, MESSAGE_MIXED_1);
        assertEquals(2, model.mentions.size());
        assertEquals("Kai", model.mentions.get(0));
        assertEquals("John", model.mentions.get(1));
        assertEquals(2, model.emoticons.size());
        assertEquals("smile", model.emoticons.get(0));
        assertEquals("Smile", model.emoticons.get(1));
        assertEquals(1, model.links.size());
        HipChatMessage.HipChatLink link = model.links.get(0);
        assertEquals(LINK_HTTPS_URL, link.url);
        assertEquals(LINK_HTTPS_TITLE, link.title);
    }

    public void testMessageMixed2() {
        HipChatMessage model = mParser.parseHipChatMessageToModel(MESSAGE_MIXED_2);
        assertNotNull(model);
        assertEquals("Incorrect original message", model.originalMessage, MESSAGE_MIXED_2);
        assertEquals(2, model.mentions.size());
        assertEquals("Kai", model.mentions.get(0));
        assertEquals("John", model.mentions.get(1));
        assertEquals(2, model.emoticons.size());
        assertEquals("smile", model.emoticons.get(0));
        assertEquals("smile123", model.emoticons.get(1));
        assertEquals(1, model.links.size());
        HipChatMessage.HipChatLink link = model.links.get(0);
        assertEquals(LINK_HTTP_URL, link.url);
        assertEquals(LINK_HTTP_TITLE, link.title);
    }
}
