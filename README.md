
# Atlassian Homework
This project simulates a simple task in HipChat app - extracting special content like mentions, emoticons and links from HipChat message.

Special Content rules:
1. @mentions - A way to mention a user. Always starts with an '@' and ends when hitting a non-word character. (http://help.hipchat.com/knowledgebase/articles/64429-how-do-mentions-work-)

2. Emoticons - For this exercise, you only need to consider 'custom' emoticons which are alphanumeric strings, no longer than 15 characters, contained in parenthesis. You can assume that anything matching this format is an emoticon. (https://www.hipchat.com/emoticons)

3. Links - Any URLs contained in the message, along with the page's title.

For example, calling your function with the following inputs should result in the corresponding return values.

Input: "@chris you around?"

Return (string):
```
{
  "mentions": [
    "chris"
  ]
}
```
 
Input: "Good morning! (megusta) (coffee)"

Return (string):
```
{
  "emoticons": [
    "megusta",
    "coffee"
  ]
}
```
 

Input: "Olympics are starting soon; http://www.nbcolympics.com"

Return (string):
```
{
  "links": [
    {
      "url": "http://www.nbcolympics.com",
      "title": "NBC Olympics | 2014 NBC Olympics in Sochi Russia"
    }
  ]
}
```

Input: "@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016"

Return (string):
```
{
  "mentions": [
    "bob",
    "john"
  ],
  "emoticons": [
    "success"
  ],
  "links": [
    {
      "url": "https://twitter.com/jdorfman/status/430511497475670016",
      "title": "Twitter / jdorfman: nice @littlebigdetail from ..."
    }
  ]
}
```
## Contributing
1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## Credits
Trinh Le
[Blog](http://icetea09.com)
## License
Copyright 2015 Trinh Le

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.